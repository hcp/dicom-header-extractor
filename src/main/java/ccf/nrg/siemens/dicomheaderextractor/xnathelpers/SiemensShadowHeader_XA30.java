/*
 * web: org.nrg.xnat.helpers.dicom.SiemensShadowHeader
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2017, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package ccf.nrg.siemens.dicomheaderextractor.xnathelpers;

//
// NOTE!!! This package was pulled in from XNAT-WEB
// org.nrg.xnat.helpers.dicom (2023/05/08)  
//

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.util.TagUtils;
import org.nrg.xft.XFTTable;

import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.Set;
import java.util.Scanner;

@Slf4j
public class SiemensShadowHeader_XA30 {
    private static final String IMAGE_NUM_4 = "IMAGE NUM 4",
    SIEMENS_CSA_HEADER = "SIEMENS CSA HEADER",
    SPEC_NUM_4 = "SPEC NUM 4",
    SIEMENS_CSA_NON_IMAGE = "SIEMENS CSA NON-IMAGE",
    SV10_MAGIC = "SV10",
    X_PROTOCOL = "<XProtocol>";

    private static final int INT32_SIZE = 4;

    private static String getStringZ(final byte[] bytes, final int offset, final int length) {
        for (int i = 0; i < length; i++) {
            if (0 == bytes[offset+i]) {
                return new String(bytes, offset, i);
            }
        }
        return new String(bytes, offset, length);
    }
    
    private static int getLEint(final ByteBuffer bb, final int offset) {
        return Integer.reverseBytes(bb.getInt(offset));
    }

    public static boolean isShadowHeader(final DicomObject o, final DicomElement e) {
        final int tag = e.tag();
        if (0x00290000 != (tag & 0xffff0000)) {
            return false;
        }
        final String version = o.getString(0x00291008);
        final String csa = o.getString(0x00290010);

        if (IMAGE_NUM_4.equals(version) && SIEMENS_CSA_HEADER.equals(csa)
                && (tag == 0x00291010 || tag == 0x00291020)) {
            return true;
        } else if (SPEC_NUM_4.equals(version) && SIEMENS_CSA_NON_IMAGE.equals(csa)
                && (tag == 0x00291210 || tag == 0x00291220
                        || tag == 0x00291110 || tag == 0x00291120)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isXprotocolHeader(final DicomObject o, final DicomElement e) {
        final int tag = e.tag();
        if (0x00210000 != (tag & 0xffff0000)) {
            return false;
        }
        if (tag == 0x00211019) {
            return true;
        } else {
            return false;
        }
    }

    public static XFTTable addRows(final XFTTable table, final DicomObject o,
            final DicomElement elem, Set<String> only) {
        final byte[] bytes = elem.getBytes();
        final String magicNumber = new String(bytes, 0, 4);
        final String xProtocol = new String(bytes, 0, 11);
        if (SV10_MAGIC.equals(magicNumber)) {
        	return addSV10Rows(table, o, elem, only, bytes);
        } else if (X_PROTOCOL.equals(xProtocol)) {
        	return addXprotocolRows(table, o, elem, only, bytes);
        } else { 
            throw new IllegalArgumentException("not a Siemens shadow header: wrong magic");
        }
    }

    private static XFTTable addSV10Rows(XFTTable table, DicomObject o, DicomElement elem, Set<String> only, byte[] bytes) {
        final ByteBuffer bb = ByteBuffer.wrap(bytes);
		final int tag = elem.tag();
        final String tagName = TagUtils.toString(tag);
        final String version = o.getString(0x00291008);
        final String csa = o.getString(0x00290010);
        final String desc;
        if (csa == null || version == null) {
            throw new IllegalArgumentException("not a Siemens shadow header: missing csa or version");
        }
        if (IMAGE_NUM_4.equals(version)) {
            if (SIEMENS_CSA_HEADER.equals(csa)) {
                if (tag == 0x00291010) {
                    desc = "Siemens CSA Image Instance Shadow Header";
                } else if (tag == 0x00291020) {
                    desc = "Siemens CSA Image Series Shadow Header";
                } else {
                    desc = "Siemens CSA Unknown Image Shadow Header";
                }
            } else {
                throw new IllegalArgumentException("invalid Siemens CSA HEADER identifier " + csa);
            }
        } else if (SPEC_NUM_4.equals(version)) {
            if (SIEMENS_CSA_NON_IMAGE.equals(csa)) {
                if (tag == 0x00291210) {
                    desc = "Siemens CSA Non-Image Instance Shadow Header";
                } else if (tag == 0x00291220) {
                    desc = "Siemens CSA Non-Image Series Shadow Header";
                } else if (tag == 0x00291110) {
                    desc = "Siemens CSA VB13 Instance Shadow Header";
                } else if (tag == 0x00291120) {
                    desc = "Siemens CSA VB13 Series Shadow Header";
                } else {
                    desc = "Siemens CSA Unknown Non-Image Shadow Header";
                }
            } else {
                throw new IllegalArgumentException("invalid Siemens CSA HEADER identifier " + csa);
            }
        } else {
            throw new IllegalArgumentException("invalid NUMARIS version " + version);
        }

        final int n = getLEint(bb, 8);
        int offset = 16;    // skip unknown int32 (= 77)
        for (int i = 0; i < n; i++) {
            final String fieldName = getStringZ(bytes, offset, 64); offset += 64;
            int vm = getLEint(bb, offset); offset += INT32_SIZE;
            final String vr = getStringZ(bytes, offset, 4); offset += 4;
            offset += INT32_SIZE;    // skip SyngoDT (int32)
            final int numItems = getLEint(bb, offset);  offset += INT32_SIZE;
            if (0 == vm) {
                vm = numItems; // can happen in spectroscopy or VB13 images
            }

            final String value;
            if (numItems > 1) {
                final Collection<String> values = Lists.newArrayListWithExpectedSize(numItems);
                offset += INT32_SIZE;   // skip unknown int32 (= 77)
                for (int j = 0; j < vm; j++) {
                    offset += 3 * INT32_SIZE;   // skip three int32s
                    final int fieldWidth = 4*(int)Math.ceil(getLEint(bb, offset)/4.0); offset += INT32_SIZE;
                    values.add(getStringZ(bytes, offset, fieldWidth).trim());
                    offset += fieldWidth;
                }
                value = DicomHeaderDump_XA30.escapeHTML(Joiner.on("\\").join(values));
            } else {
                value = "";
            }

            // skip the junk bytes at the end of the item
            if (numItems < 1) {
                offset += 4;
            } else if (numItems < vm) {
                offset += 16;
            } else {
                offset += 16 * (numItems - vm);
            }

            if (null == only || only.isEmpty() || only.contains(fieldName)) {
                table.insertRow(new String[]{tagName, fieldName, vr, value, desc});
            }
        }
        return table;
	}

	private static XFTTable addXprotocolRows(final XFTTable table, final DicomObject o, final DicomElement elem, 
    		final Set<String> only, final byte[] bytes) {
		final int tag = elem.tag();
        final String tagName = TagUtils.toString(tag);
    	final Scanner reader = new Scanner(new String(bytes));
        final String desc = "Siemens XProtocol Image Shadow Header";
    	boolean inAscconv = false;
    	while (reader.hasNextLine()) {
    		final String line = reader.nextLine();
    		if (line.contains("ASCCONV BEGIN")) inAscconv = true;
    		else if (line.contains("ASCCONV END")) inAscconv = false;
    		else if (inAscconv) {
    			final String[] splitStr = line.split("\\s+[=]\\s+");
    			if (splitStr.length==2) {
    				table.insertRow(new String[]{tagName, splitStr[0], "UN", splitStr[1], desc});
    			} else {
    				log.error("ERROR:  Unexpected array length (XProtocol header):  " + splitStr.length + 
    						" - (ROW=" + line + ")");
    			}
    		}
    	}
		return table;
	}
}
