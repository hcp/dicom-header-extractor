package ccf.nrg.siemens.dicomheaderextractor.exception;

public class DicomHeaderExtractionException extends Exception {
	
	private static final long serialVersionUID = -8400187490764528779L;

	public DicomHeaderExtractionException() {
		super();
	}

	public DicomHeaderExtractionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DicomHeaderExtractionException(String message, Throwable cause) {
		super(message, cause);
	}

	public DicomHeaderExtractionException(String message) {
		super(message);
	}

	public DicomHeaderExtractionException(Throwable cause) {
		super(cause);
	}

}
