package ccf.nrg.siemens.dicomheaderextractor.util;

import java.io.File;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import ccf.nrg.siemens.dicomheaderextractor.client.Dcm2niixBidsClient;
import ccf.nrg.siemens.dicomheaderextractor.exception.DicomHeaderExtractionException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NiftiBidsUtil {
	
	final static Dcm2niixBidsClient _dcm2niixClient = new Dcm2niixBidsClient();

	public static List<Map<String,Object>> getDcm2NiixBidsInfo(File baseDir) {
		try {
			return _dcm2niixClient.getDcm2niixBidsInfo(baseDir);
		} catch (DicomHeaderExtractionException e) {
			log.error(e.toString());
			return new ArrayList<>();
		}
	}

}
