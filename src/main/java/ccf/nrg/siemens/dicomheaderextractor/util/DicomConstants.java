package ccf.nrg.siemens.dicomheaderextractor.util;

/**
 * The Class DicomConstants.
 *
 * @author Atul
 */
public class DicomConstants {
	/* *****************************************
	 * Common Non-specific values.
	 * *****************************************/
	/** The Constant BACKSLASH. */
	public static final String BACKSLASH = "\\";

	/** The Constant EMPTY_STRING. */
	public static final String EMPTY_STRING = "";

	/** The Constant SPACE_STRING. */
	public static final String SPACE_STRING = " ";
	
	/** The Constant UNDERSCORE. */
	public static final String UNDERSCORE = "_";

	/** Default stud value for unreachable values */
	public static final String UNKNOWN = "UNKNOWN";

	/** The Constant NEW_LINE. */
	public static final String NEW_LINE = "\n";

	/* *****************************************
	 * DICOM non-specific placeholder values and
	 * keys.
	 * *****************************************/
	/** The Constant VALUE. */
	public static final String VALUE = "value";

	/** The Constant TAG2. */
	public static final String TAG2 = "tag2";

	/** The Constant TAG1. */
	public static final String TAG1 = "tag1";

	/* *****************************************
	 * DICOM specific prefix and suffix
	 * annotations.
	 * *****************************************/
	/** The Constant ASCCONV_END. */
	public static final String ASCCONV_END = "### ASCCONV END";

	/** The Constant ASCCONV_BEGIN. */
	public static final String ASCCONV_BEGIN = "### ASCCONV BEGIN";

	/* *****************************************
	 * DICOM specific tags and key mapping
	 * values.
	 * *****************************************/

	 /** The DICOM header tag for Acquisition Number. */
	 public static final String ACQUISITION_NUMBER_TAG = "(0020,0012)";

	/** The Constant ACQUISITION_TIME_TAG. */
	public static final String ACQUISITION_TIME_TAG = "(0008,0032)";

	/** The Constant ACQUISITION_TIME_KEY. */
	public static final String ACQUISITION_TIME_KEY = "Acquisition_Time";

	 /** The Diffusion B Value tag for VE scanner types. */
	 public static final String DIFFUSION_B_TAG_VE = "(0019,100C)";
 
	 /** The Diffusion B Value key mapping for VE scanner types. */
	 public static final String DIFFUSION_B_KEY_VE = "DiffusionBValue";
 
	 /** The Diffusion B Value tag for XA scanner types. */
	 public static final String DIFFUSION_B_TAG_XA = "(0018,9087)";
 
	 /** The Diffusion B Value key mapping for XA scanner types. */
	 public static final String DIFFUSION_B_KEY_XA = "DiffusionBValue_XA";

	/** The Constant ECHO_SPACING_SEC. */
	public static final String ECHO_SPACING_SEC = "Echo_Spacing_(sec)";

	/** The Constant FMRI_EXTERNAL_INFO. */
	public static final String FMRI_EXTERNAL_INFO = "FmriExternalInfo";

	/** The Constant FMRI_VERSION_ICE_LINUX. */
	public static final String FMRI_VERSION_ICE_LINUX = "fMRI_Version_ICE_Linux";

	/** The Constant FMRI_VERSION_ICE_WIN32. */
	public static final String FMRI_VERSION_ICE_WIN32 = "fMRI_Version_ICE_Win32";

	/** The Constant FMRI_VERSION_SEQUENCE. */
	public static final String FMRI_VERSION_SEQUENCE = "fMRI_Version_Sequence";

	/** The Constant IMAGE_COMMENTS_CODE. */
	public static final String IMAGE_COMMENTS_TAG = "(0020,4000)";

	/** The Constant IMAGE_COMMENTS_KEY. */
	public static final String IMAGE_COMMENTS_KEY = "Image_Comments";

	/** The Constant IMAGE_INSTANCE_ID_CODE. */
	public static final String INSTANCE_NUMBER_TAG = "(0020,0013)";

	/** The Constant MR_PHOENIX_PROTOCOL. */
	public static final String MR_PHOENIX_PROTOCOL = "MrPhoenixProtocol";

	/** The Constant READOUT_SAMPLE_SPACING_NUMBER_TAG. */
	public static final String READOUT_SAMPLE_SPACING_NUMBER_TAG = "(0019,1018)";

	/** The Constant S_GRADSPEC_AL_SHIM_CURRENT. */
	public static final String S_GRADSPEC_AL_SHIM_CURRENT = "sGRADSPEC.alShimCurrent";

	/** The Constant S_GRADSPEC_AL_SHIM_CURRENT_4. */
	public static final String S_GRADSPEC_AL_SHIM_CURRENT_4 = "sGRADSPEC.alShimCurrent[4]";

	/** The Constant S_GRADSPEC_AL_SHIM_CURRENT_3. */
	public static final String S_GRADSPEC_AL_SHIM_CURRENT_3 = "sGRADSPEC.alShimCurrent[3]";

	/** The Constant S_GRADSPEC_AL_SHIM_CURRENT_2. */
	public static final String S_GRADSPEC_AL_SHIM_CURRENT_2 = "sGRADSPEC.alShimCurrent[2]";

	/** The Constant S_GRADSPEC_AL_SHIM_CURRENT_1. */
	public static final String S_GRADSPEC_AL_SHIM_CURRENT_1 = "sGRADSPEC.alShimCurrent[1]";

	/** The Constant S_GRADSPEC_AL_SHIM_CURRENT_0. */
	public static final String S_GRADSPEC_AL_SHIM_CURRENT_0 = "sGRADSPEC.alShimCurrent[0]";

	/** The Constant S_GRADSPEC_AS_GPA_DATA_0_L_OFFSET_Z. */
	public static final String S_GRADSPEC_AS_GPA_DATA_0_L_OFFSET_Z = "sGRADSPEC.asGPAData[0].lOffsetZ";

	/** The Constant S_GRADSPEC_AS_GPA_DATA_0_L_OFFSET_Y. */
	public static final String S_GRADSPEC_AS_GPA_DATA_0_L_OFFSET_Y = "sGRADSPEC.asGPAData[0].lOffsetY";

	/** The Constant S_GRADSPEC_AS_GPA_DATA_0_L_OFFSET_X. */
	public static final String S_GRADSPEC_AS_GPA_DATA_0_L_OFFSET_X = "sGRADSPEC.asGPAData[0].lOffsetX";

	/** The Constant SIEMENS_GRADSPEC_L_OFFSET. */
	public static final String SIEMENS_GRADSPEC_L_OFFSET = "Siemens GRADSPEC lOffset";
}
