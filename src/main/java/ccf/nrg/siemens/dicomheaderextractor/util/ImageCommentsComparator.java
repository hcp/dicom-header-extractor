package ccf.nrg.siemens.dicomheaderextractor.util;

import java.util.Comparator;

/**
 * The Class ImageCommentsComparator.
 * @author Atul
 */
public class ImageCommentsComparator implements Comparator<ImageComments> {

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(ImageComments o1, ImageComments o2) {
		return o1.getIndex()>o2.getIndex() ?1:0;
	}

}
