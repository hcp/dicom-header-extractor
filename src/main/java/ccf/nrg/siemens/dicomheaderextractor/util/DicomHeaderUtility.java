/**
 * 
 */
package ccf.nrg.siemens.dicomheaderextractor.util;

import java.io.IOException;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.nrg.xft.XFTTable;

/**
 * The Class DicomHeaderUtility.
 * @author Atul
 */
public class DicomHeaderUtility {

	/** The logger. */
	private static Logger logger = Logger.getLogger(DicomHeaderUtility.class.getName());

	/** The Constant acqMatrixPattern. */
	private final static Pattern acqMatrixPattern = Pattern.compile("\\A(\\d|[1-9]\\d+)p?\\*(\\d|[1-9]\\d+)s?\\z");


	/**
	 * Creates the header map.
	 *
	 * @param t the t
	 * @return the map
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParseException 
	 */
	public Map<String, String> createHeaderMap(XFTTable t, Boolean skipMultiFileAttributes) throws IOException, ParseException {
		Map<String, String> headerValueMap = new HashMap<String, String>();

		Integer tag1ColIndex = t.getColumnIndex(DicomConstants.TAG1);
		Integer tag2ColIndex = t.getColumnIndex(DicomConstants.TAG2);
		Integer valueIndex = t.getColumnIndex(DicomConstants.VALUE);
		Integer keyIndex =null;
		while (t.hasMoreRows()) {
			keyIndex=tag2ColIndex;

			Object[] row = t.nextRow();
			if(row[tag2ColIndex] == null || DicomConstants.EMPTY_STRING.equals(row[tag2ColIndex])) {
				keyIndex=tag1ColIndex;
			}
			Object key = row[keyIndex];
			Object value = row[valueIndex];
			if (DicomConstants.MR_PHOENIX_PROTOCOL.equals(row[tag2ColIndex])) 
			{
				populateMRPhoenixValues(headerValueMap,value);
			}
			else if (key != null && value != null) 
				headerValueMap.put(((String)key).trim(), ((String)value).replaceAll("&amp;", "&").trim());
		}
		setFMRIValues(headerValueMap);
		setGradSpecAlShimCurrentValue(headerValueMap);
		setGradSpecLOffsetValue(headerValueMap);
		
		//As per Mike Harms comments, commenting code converting nanosecond to seconds.
		//it's breaking existing protocol validation process.
		//convertReadOutSpacingToSeconds(headerValueMap);
		
		//Populating Acquisition time as XNAT was using earliest time from Series Time and Acquisition Time which is wrong in case of Recon scans.
		convertAcquisitionTime(headerValueMap);

		if(headerValueMap.get("(0019,1028)")!=null && headerValueMap.get("(0051,100B)")!=null) 
		{
			try {
				Double bpppe = Double.valueOf(headerValueMap.get("(0019,1028)"));
				Integer nsamples=0;
				final Matcher matcher;
				matcher = acqMatrixPattern.matcher(headerValueMap.get("(0051,100B)"));
				if (matcher.matches()) 
				{
					nsamples = Integer.valueOf(matcher.group(1));
					headerValueMap.put(DicomConstants.ECHO_SPACING_SEC, Double.toString(1/(bpppe * nsamples)));
				}
			} catch (NumberFormatException e) {
				logger.error("NumberFormatException thrown at 0019,1029. ECHO_SPACING_SEC cannot be calculated.", e);
			}
		}
		if(skipMultiFileAttributes)
			skipMultiFileAttributes(headerValueMap);
		return headerValueMap;	
	}

	/**
	 * Skip multi file attributes.
	 *
	 * @param headerValueMap the header value map
	 */
	private void skipMultiFileAttributes(Map<String, String> headerValueMap) {
		headerValueMap.remove(DicomConstants.ACQUISITION_TIME_TAG);
		headerValueMap.remove(DicomConstants.DIFFUSION_B_TAG_VE);
		headerValueMap.remove(DicomConstants.IMAGE_COMMENTS_TAG);
	}

	/**
	 * @param headerValueMap
	 */
	private void convertReadOutSpacingToSeconds(Map<String, String> headerValueMap) {
		String val=headerValueMap.get(DicomConstants.READOUT_SAMPLE_SPACING_NUMBER_TAG);
		if(val!=null && !DicomConstants.EMPTY_STRING.equals(val) && Long.parseLong(val)>0)
		{
			headerValueMap.put(DicomConstants.READOUT_SAMPLE_SPACING_NUMBER_TAG, new DecimalFormat("0.#E0").format(Double.parseDouble(val)*1E-9));	
		}
	}
	
	/**
	 * Convert acquisition time from HHmmss:SSSSSS to HH:mm:ss format.
	 *
	 * @param headerValueMap the header value map
	 * @throws ParseException the parse exception
	 */
	private void convertAcquisitionTime(Map<String, String> headerValueMap) throws ParseException {
		String val=headerValueMap.get(DicomConstants.ACQUISITION_TIME_TAG);
		if(val!=null && !DicomConstants.EMPTY_STRING.equals(val))
		{
			headerValueMap.put(DicomConstants.ACQUISITION_TIME_TAG, parseTime(val).toString());	
		}
	}

	/**
	 * Sets the FMRI values.
	 *
	 * @param headerValueMap the header value map
	 */
	private void setFMRIValues(Map<String, String> headerValueMap) {
		String val=headerValueMap.get(DicomConstants.FMRI_EXTERNAL_INFO);
		if(val!=null && !DicomConstants.EMPTY_STRING.equals(val))
		{
			String[] temp=val.split("\\|\\|");
			headerValueMap.put("UUID", temp[0]);
			headerValueMap.put(DicomConstants.FMRI_VERSION_SEQUENCE, temp[1].substring(temp[1].indexOf(":")+1, temp[1].length()).trim());
			headerValueMap.put(DicomConstants.FMRI_VERSION_ICE_WIN32, temp[2].substring(temp[2].indexOf(":")+1, temp[2].length()).trim());
			headerValueMap.put(DicomConstants.FMRI_VERSION_ICE_LINUX, temp[3].substring(temp[3].indexOf(":")+1, temp[3].length()).trim());
		}
	}

	/**
	 * Sets the grad spec al shim current value.
	 *
	 * @param headerValueMap the header value map
	 */
	private void setGradSpecAlShimCurrentValue(Map<String, String> headerValueMap) {
		String value0=headerValueMap.get(DicomConstants.S_GRADSPEC_AL_SHIM_CURRENT_0);
		String value1=headerValueMap.get(DicomConstants.S_GRADSPEC_AL_SHIM_CURRENT_1);
		String value2=headerValueMap.get(DicomConstants.S_GRADSPEC_AL_SHIM_CURRENT_2);
		String value3=headerValueMap.get(DicomConstants.S_GRADSPEC_AL_SHIM_CURRENT_3);
		String value4=headerValueMap.get(DicomConstants.S_GRADSPEC_AL_SHIM_CURRENT_4);
		String value=value0+DicomConstants.BACKSLASH+value1+DicomConstants.BACKSLASH+value2+DicomConstants.BACKSLASH+value3+DicomConstants.BACKSLASH+value4;
		if((!"\\\\\\\\".equals(value)) && (!"\\\\\\\\".equals(value.replaceAll("null", ""))))
			headerValueMap.put(DicomConstants.S_GRADSPEC_AL_SHIM_CURRENT,value.replaceAll("null", ""));
	}

	/**
	 * Sets the grad spec L offset value.
	 *
	 * @param headerValueMap the header value map
	 */
	private void setGradSpecLOffsetValue(Map<String, String> headerValueMap) {
		String valueX=headerValueMap.get(DicomConstants.S_GRADSPEC_AS_GPA_DATA_0_L_OFFSET_X);
		String valueY=headerValueMap.get(DicomConstants.S_GRADSPEC_AS_GPA_DATA_0_L_OFFSET_Y);
		String valueZ=headerValueMap.get(DicomConstants.S_GRADSPEC_AS_GPA_DATA_0_L_OFFSET_Z);
		String iOffSetValue=valueX+DicomConstants.BACKSLASH+valueY+DicomConstants.BACKSLASH+valueZ;
		if(!"\\\\".equals(iOffSetValue) && (!"\\\\".equals(iOffSetValue.replaceAll("null", ""))))
			headerValueMap.put(DicomConstants.SIEMENS_GRADSPEC_L_OFFSET,iOffSetValue.replaceAll("null", ""));
	}

	/**
	 * Populate MR phoenix values.
	 *
	 * @param headerValueMap the header value map
	 * @param value the value
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void populateMRPhoenixValues(Map<String, String> headerValueMap,Object value) throws IOException {
		Properties props = new Properties();
		String mrPhoenix = (String) value;
		final int ascconv0 = mrPhoenix.indexOf(DicomConstants.ASCCONV_BEGIN);
		if (ascconv0 < 0) {
			logger.info("no ASCCONV found in Siemens MR Phoenix");
		}
		final int props0 = mrPhoenix.indexOf(DicomConstants.NEW_LINE, ascconv0) + 2;
		if (props0 < 2) {
			logger.info("no CR after start of Siemens MR Phoenix ASCCONV");
		}
		final int ascconv1 = mrPhoenix.indexOf(DicomConstants.ASCCONV_END, props0);
		if (ascconv1 < 0) {
			logger.info("no end to Siemens MR Phoenix ASCCONV");
		}
		final String proptext = mrPhoenix.substring(props0, ascconv1).replaceAll("\\n", DicomConstants.NEW_LINE).replaceAll("\\t",
				"\t").replaceAll("&quot;", "\"").replaceAll("&amp;", "&");
		props.load(new StringReader(proptext));
		headerValueMap.putAll((Map) props);
	}
	
	/**
	 * Parses the time.
	 *
	 * @param a the a
	 * @return the string
	 * @throws ParseException the parse exception
	 */
	public String parseTime(final String a) throws ParseException {
   
        final String format = "%1$2s:%2$2s:%3$2s";
        final String xsdtime;
        if (a.indexOf(':') > 0) {
            // looks like old DICOM format
            if (a.length() >= 8 && a.charAt(2) == ':' && a.charAt(4) == ':') {
                xsdtime = a.substring(0,9);    // essentially same as XML format
            } else {
                throw new ParseException("Unable to Parse time. Invalid Time ::"+a, 0);
            }
        } else if (a.length() >= 6) {
            xsdtime = String.format(format, a.substring(0,2), a.substring(2,4), a.substring(4,6));
        } else {
            throw new ParseException("Unable to Parse time. Invalid Time ::"+a, 0);

        }
        return xsdtime;
    }
}
