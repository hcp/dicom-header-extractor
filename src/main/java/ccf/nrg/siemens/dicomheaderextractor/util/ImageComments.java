package ccf.nrg.siemens.dicomheaderextractor.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


/**
 * The Class ImageComments.
 * @author Atul
 */


@Getter @Setter @AllArgsConstructor
public class ImageComments {
	
	 /** The index. */
 	private int index;
	 
 	/** The value. */
 	private String value;
}
