package ccf.nrg.siemens.dicomheaderextractor.util;

import lombok.extern.slf4j.Slf4j;

import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatAddfieldI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.*;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.ActionNameAbsent;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.IDAbsent;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.JustificationAbsent;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;

import ccf.nrg.siemens.dicomheaderextractor.SiemensDicomHeaderExtractor;

import java.io.File;
import java.util.*;
import java.util.Map.Entry;

@Slf4j
public class DicomExtractToScanMetadataUtil {

    private static String DICOM_ORIG = "DICOM_ORIG";
    private static String DICOM = "DICOM";
    private static String secondary = "secondary";

    public static void processExperimentData(XnatImagesessiondata imageSession, UserI user) {
        final List<XnatImagescandataI> scans = new ArrayList<>();
        scans.addAll(imageSession.getSortedScans());
        for (final XnatImagescandataI scan : scans) {
            if (scan instanceof XnatMrscandata) {
                processScan((XnatMrscandata) scan, imageSession, user);
            }
        }
    }

    public static void processExperimentData(String dataId, UserI user) {
        final XnatImagesessiondata imageSession = XnatImagesessiondata.getXnatImagesessiondatasById(dataId, user, false);
        final List<XnatImagescandataI> scans = new ArrayList<>();
        scans.addAll(imageSession.getSortedScans());
        for (final XnatImagescandataI scan : scans) {
            if (scan instanceof XnatMrscandata) {
                processScan((XnatMrscandata) scan, imageSession, user);
            }
        }
    }

    private static void processScan(XnatMrscandata scan, XnatExperimentdata experimentData, UserI user) {
    	final SiemensDicomHeaderExtractor dicomHeaderGen = new SiemensDicomHeaderExtractor();
        try {
        	final File baseDir = determineBaseDir(scan);
            log.debug((baseDir!=null) ? baseDir.getAbsolutePath() : "ERROR:  baseDir for scan " + scan.getId() + " is NULL!!!");

            if (baseDir != null && baseDir.exists()) {
                final Map<String, String> siemensValuesMap = dicomHeaderGen.getSiemensDicomHeaderInfoMap(baseDir.getAbsolutePath());
                final List<Map<String,Object>> niftiInfo = NiftiBidsUtil.getDcm2NiixBidsInfo(baseDir);

                updateScanWithDicomData(scan, niftiInfo, siemensValuesMap, user);
            } else {
                log.error("Scan directories do not exist for " + baseDir);
            }
        } catch (Exception e) {
			log.error("ERROR:  Error processing scan (scan=" + scan.getId() + ")", e);
        }
    }

    private static File determineBaseDir(XnatMrscandata scan) {
    	XnatResourcecatalog resource = null;
    	if (ResourceUtils.hasResource(scan, DICOM_ORIG)) {
    		resource = ResourceUtils.getResource(scan, DICOM_ORIG);
    	} else if (ResourceUtils.hasResource(scan, DICOM)) {
    		resource = ResourceUtils.getResource(scan, DICOM);
    	} else if (ResourceUtils.hasResource(scan, secondary)) {
    		resource = ResourceUtils.getResource(scan, secondary);
    	} else {
    		resource = findAnyDicomResource(scan);
    	}
    	return (resource != null) ? ResourceUtils.getResourcePathFile(resource) : null;
    }

    private static XnatResourcecatalog findAnyDicomResource(XnatMrscandata scan) {
    	for (final XnatAbstractresourceI resource : ResourceUtils.getResources(scan)) {
    		if (!(resource instanceof XnatResourcecatalog)) {
    			continue;
    		}
    		if (ResourceUtils.getFilesByFileNameRegex(resource, "(?i).*[.]dcm$").size()>0 ||
    				(ResourceUtils.getFiles(resource).size()>0 && 
    						ResourceUtils.getFilesByFileNameRegex(resource, "(?i).*[.].*$").size()==0)) {
    			return (XnatResourcecatalog)resource;
    		}
    	}
    	return null;
	}

	@SuppressWarnings("unused")
    private static String getStringValueFromValuesMap(Map<String, String> valuesMap, String valueKey) {
    		final Object objectValue = getObjectValueFromValuesMap(valuesMap, valueKey);
    		return (objectValue!=null) ? objectValue.toString() : null;
	}

    private static Object getObjectValueFromValuesMap(Map<String, String> valuesMap, String valueKey) {
    		//log.debug(valueKey + " - 1");
    		Object mapValue = null;
    		try { 
    			mapValue = (valueKey != null) ? valuesMap.get(valueKey) : null;
    		} catch (Exception e) {
    			log.error("Exception thrown pulling " + valueKey);
    		}
  	        valuesMap.remove(valueKey);
    		return mapValue;
	}
    
    private static Object getObjectValueFromNiftiInfo(List<Map<String, Object>> niftiInfo, String niftiKey) {
   		for (final Map<String, Object> nMap : niftiInfo) {
   			for (final String key : nMap.keySet()) {
   				if (key != null && nMap.get(key) != null && niftiKey.equals(key)) {
   					return nMap.get(key);
   				}
   			}
   		}
   		return null;
	}

    public static String getStringValue(Map<String, String> dicomMap, String dicomKey, List<Map<String, Object>> niftiInfo, List<String>niftiKeys) {
    	Object objectValue = getObjectValue(dicomMap, dicomKey, niftiInfo, niftiKeys, null, true);
    	return (objectValue != null) ? objectValue.toString() : null; 
    }

    @SuppressWarnings("unused")
	private static String getStringValue(Map<String, String> dicomMap, String dicomKey, List<Map<String, Object>> niftiInfo, List<String> niftiKeys, Object currentValue, boolean overwriteExisting) {
    	Object objectValue = getObjectValue(dicomMap, dicomKey, niftiInfo, niftiKeys, currentValue, overwriteExisting);
    	return (objectValue != null) ? objectValue.toString() : ""; 
	}

    @SuppressWarnings("unused")
    private static Object getObjectValue(Map<String, String> dicomMap, String dicomKey, List<Map<String, Object>> niftiInfo, List<String> niftiKeys) {
    	return getObjectValue(dicomMap, dicomKey, niftiInfo, niftiKeys, null, true);
    }

    private static Object getObjectValue(Map<String, String> dicomMap, String dicomKey, List<Map<String, Object>> niftiInfo, List<String> niftiKeys, Object currentValue, boolean overwriteExisting) {
    		//log.debug(dicomKey + " - 1");
 			if (!overwriteExisting && currentValue != null && !currentValue.toString().trim().equals("")) {
 				dicomMap.remove(dicomKey);
 				return currentValue;
 			}
    		Object returnValue = (dicomMap != null && dicomKey != null && !dicomKey.trim().isEmpty()) ?
    				getObjectValueFromValuesMap(dicomMap, dicomKey) : null;
    		if (returnValue == null && niftiInfo != null) {
    			for (final String niftiKey : niftiKeys) {
    				returnValue = getObjectValueFromNiftiInfo(niftiInfo, niftiKey);
    				if (returnValue != null) {
    					log.debug("DEBUG:  Using niftiInfo for value (niftiKey=" + niftiKey + ", value=" + returnValue + ")");
    					return returnValue;
    				}
    			}
    		}
    		return (returnValue != null) ? returnValue : currentValue; 
	}

    public static String getStringValue(Map<String, String> dicomMap, String dicomKey, List<Map<String, Object>> niftiInfo, String niftiKey) {
    	final List<String> niftiKeys = new ArrayList<String>();
    	if ( niftiKey != null && !niftiKey.isEmpty() ) niftiKeys.add(niftiKey);
    	return getStringValue(dicomMap, dicomKey, niftiInfo, niftiKeys);
    }

	public static String getStringValue(Map<String, String> dicomMap, String dicomKey, List<Map<String, Object>> niftiInfo, String niftiKey, Object currentValue, boolean overwriteExisting) {
    	final List<String> niftiKeys = new ArrayList<String>();
    	if ( niftiKey != null && !niftiKey.isEmpty() ) niftiKeys.add(niftiKey);
    	Object objectValue = getObjectValue(dicomMap, dicomKey, niftiInfo, niftiKeys, currentValue, overwriteExisting);
    	return (objectValue != null) ? objectValue.toString() : null; 
	}

    @SuppressWarnings("unused")
	private static Object getObjectValue(Map<String, String> dicomMap, String dicomKey, List<Map<String, Object>> niftiInfo, String niftiKey) {
    	final List<String> niftiKeys = new ArrayList<String>();
    	if ( niftiKey != null && !niftiKey.isEmpty() ) niftiKeys.add(niftiKey);
    	return getObjectValue(dicomMap, dicomKey, niftiInfo, niftiKeys, null, true);
    }

    private static Object getObjectValue(Map<String, String> dicomMap, String dicomKey, List<Map<String, Object>> niftiInfo, String niftiKey, Object currentValue, boolean overwriteExisting) {
    	final List<String> niftiKeys = new ArrayList<String>();
    	if ( niftiKey != null && !niftiKey.isEmpty() ) niftiKeys.add(niftiKey);
    	return getObjectValue(dicomMap, dicomKey, niftiInfo, niftiKeys, currentValue, overwriteExisting);
	}

	private static Double parseDoubleFromDicomData(
		Map<String, String> siemensValuesMap,
		String dicomKey,
		List<Map<String, Object>> niftiInfo,
		String niftiKey) {

		return parseDoubleFromDicomData(
			siemensValuesMap,
			dicomKey,
			niftiInfo,
			niftiKey,
			null);
	}

	/**
	 * Attempts to extract a value from the raw
	 * data available in the nifti mappings. If
	 * the value cannot be extracted from the
	 * DICOM headers, but is available in NIFTI,
	 * and a mapping function is provided as
	 * `niftiParser`, the value is converted to
	 * the expected value.
	 * @param siemensValuesMap
	 * @param dicomKey
	 * @param niftiInfo
	 * @param niftiKey
	 * @param niftiParser
	 */
	private static Double parseDoubleFromDicomData(
		Map<String, String> siemensValuesMap,
		String dicomKey,
		List<Map<String, Object>> niftiInfo,
		String niftiKey,
		MapFunction<Double, Double> niftiParser) {

		Double extracted;
		extracted = safeDouble(getStringValue(siemensValuesMap, dicomKey, niftiInfo, ""));
		if (extracted == null) {
			extracted = safeDouble(getStringValue(siemensValuesMap, "", niftiInfo, niftiKey));
			extracted = (niftiParser != null) ? niftiParser.doMap(extracted) : extracted;
		}
		return extracted;
	}

	private static void updateScanWithDicomData(XnatMrscandata scan, List<Map<String, Object>> niftiInfo, Map<String, String> siemensValuesMap, UserI user) {
		updateScanWithXNATSpecificDicomData(scan, niftiInfo, siemensValuesMap);
		// Now, deal with addParam fields that require special handling.
		updateScanWithAdditionalDicomData(scan, niftiInfo, siemensValuesMap);
		saveScanWithDicomData(scan, user);
    }

	/**
	 * Sets values on the given scan specific to XNAT's scan schema
	 * extracted from DICOM headers and NIFTI data.
	 * @param scan
	 * @param niftiInfo
	 * @param siemensValuesMap
	 */
	private static void updateScanWithXNATSpecificDicomData(XnatMrscandata scan, List<Map<String, Object>> niftiInfo, Map<String, String> siemensValuesMap) {
		// NOTE: getStringValue and getObjectValue remove the value
		// from the siemensValuesMap if it can be found and returned.

		// The ordering of the set calls in this method, and others
		// like it, dictates how/where these fields are applied to the
		// resulting XML.
		//log.debug("Scan to save (scan=" + scan.getItem().toXML_String() + ")");
		scan.setSeriesDescription(getStringValue(siemensValuesMap,"Series_Description",niftiInfo,"SeriesDescription",scan.getSeriesDescription(),false));
    	scan.setModality(getStringValue(siemensValuesMap,"Modality",niftiInfo,"Modality",scan.getModality(),false));
    	scan.setStarttime(getObjectValue(siemensValuesMap,"Acquisition_Time", niftiInfo, "AcquisitionTime",scan.getStarttime(),false));
    	scan.setParameters_acqtype(getStringValue(siemensValuesMap,"Acquisition_type", niftiInfo, "MRAcquisitionType"));
    	scan.setParameters_scansequence(getStringValue(siemensValuesMap,"Scan_sequence",niftiInfo,"ScanningSequence"));;
		scan.setParameters_seqvariant(getStringValue(siemensValuesMap,"Sequence_variant",niftiInfo,"SequenceVariant"));
		scan.setParameters_scanoptions(getStringValue(siemensValuesMap,"Scan_options",niftiInfo,"ScanOptions"));
		scan.setParameters_subjectposition(getStringValue(siemensValuesMap,"Subject_position",niftiInfo,"PatientPosition"));
		scan.setScanner_softwareversion(getStringValue(siemensValuesMap,"Software_Version(s)", niftiInfo, "SoftwareVersions"));

		// Pixel Bandwidth.
		Double pixelBandwidth = safeDouble(getStringValue(siemensValuesMap,"Pixel_bandwidth",niftiInfo,"PixelBandwidth"));
		scan.setParameters_pixelbandwidth(pixelBandwidth);

		// In Plane Phase Encoding Direction.
		String ippeDirection = getStringValue(siemensValuesMap,"In-plane_phase_encoding_direction",niftiInfo,"InPlanePhaseEncodingDirectionDICOM");
		if (scannerTypeIs(scan, "XA") && ippeDirection != null && ippeDirection.equals("COLUMN")) {
			// Changing to COL for backwards compatibility and make Validation simpler.
			ippeDirection = "COL";
		}
		scan.setParameters_inplanephaseencoding_direction(ippeDirection);
		// In Plane Phase Encoding Rotation.
		scan.setParameters_inplanephaseencoding_rotation(getStringValue(siemensValuesMap,"In-plane_phase_encoding_rotation",niftiInfo,""));

		// In Plane Phase Encoding Direction Positive.
		String directionPositive;
		if (scannerTypeIs(scan, "XA")) {
			directionPositive = getStringValue(siemensValuesMap, "In-plane_phase_encoding_direction_positive_XA",niftiInfo, "");
		} else {
			directionPositive = getStringValue(siemensValuesMap,"In-plane_phase_encoding_direction_positive",niftiInfo,"");
			siemensValuesMap.remove("In-plane_phase_encoding_direction_positive_XA");
		}
		scan.setParameters_inplanephaseencoding_directionpositive(directionPositive);

		// Readout Sample Spacing.
		Double readoutSampleSpacing;
		readoutSampleSpacing = safeDouble(getStringValue(siemensValuesMap, "Readout_sample_spacing", niftiInfo, ""));
		if (readoutSampleSpacing == null) {
			readoutSampleSpacing = safeDouble(getStringValue(siemensValuesMap, "", niftiInfo, "DwellTime"));
			// the value in the JSON is in seconds and needs
			// to be converted to nanoseconds for consistency.
			readoutSampleSpacing = (readoutSampleSpacing != null) ? readoutSampleSpacing*1e9 : null;
		}
		scan.setParameters_readoutsamplespacing(safeString(readoutSampleSpacing));

		// Echo Spacing.
		scan.setParameters_echospacing(safeDouble(getStringValue(siemensValuesMap, "Echo_Spacing_(sec)", niftiInfo, "EffectiveEchoSpacing")));

		// Set Coil; different from Coil String.
		String coil = getStringValue(siemensValuesMap,"coil",niftiInfo,"ReceiveCoilName").replaceAll("\"","");
		scan.setCoil(coil);

		// Parsing for UUID as filenameUUID.
		String filenameUUID = getStringValue(siemensValuesMap,"UUID",niftiInfo,"");
		if (filenameUUID == null) {
			// Only look for filenameUUID from NIFTI if we absolutely
			// can't find it in the DICOM, and if the WipMemBlock
			// meets the following criteria. We want to see at least
			// one '|' char separating its subvalues and contains
			// 'Sequence'.
			filenameUUID = getStringValue(siemensValuesMap, "", niftiInfo, "WipMemBlock");
			if (filenameUUID != null && filenameUUID.contains("|Sequence")) {
				filenameUUID = filenameUUID.substring(0,filenameUUID.indexOf("|"));
			} else if (filenameUUID != null) {
				filenameUUID = null;
			}
		}
		scan.setFilenameuuid(filenameUUID);

		// Parsing Diffusion variables.
		scan.setParameters_diffusion_refocusflipangle(getStringValue(siemensValuesMap,"adFlipAngleDegree[1]",niftiInfo,""));
		String maxDiffusionB;
		if (scannerTypeIs(scan, "XA")) {
			maxDiffusionB =  getStringValue(siemensValuesMap, "DiffusionBValue_XA", niftiInfo, "");
		} else {
			maxDiffusionB = getStringValue(siemensValuesMap,"DiffusionBValue",niftiInfo,"");
		}
		scan.setParameters_diffusion_bmax(maxDiffusionB);

		// Use NIFTI value here because format is better.  Remove
		// DICOM value.
		scan.setFieldstrength(getStringValue(siemensValuesMap,"",niftiInfo,"MagneticFieldStrength"));
		siemensValuesMap.remove("Field_Strength");
		scan.setParameters_sequence(getStringValue(siemensValuesMap,"",niftiInfo, Arrays.asList(new String[] {"SequenceName", "PulseSequenceName"})));
		scan.setParameters_flip(safeInteger(getStringValue(siemensValuesMap,"Flip",niftiInfo,"FlipAngle")));

		// Parsing ImageType per scanner type.
		String imageType;
		// This is a raw 'get' outside normal usage in this code.
		// Doing this to preserve the mapping in case it does exist;
		// we don't want to remove it from the map before adding it
		// as an addParam, and we only care if it exists and has a
		// specific value.
		// The particular value we are looking for, 1.2.840.10008.5.1.4.1.1.4.1
		// corresponds to 'EnhancedDICOM' images.
		String mediaStorageSOPClassUID = siemensValuesMap.get("MediaStorageSOPClassUID");
		if (scannerTypeIs(scan, "XA") && mediaStorageSOPClassUID != null && mediaStorageSOPClassUID.equals("1.2.840.10008.5.1.4.1.1.4.1")) {
			imageType = getStringValue(siemensValuesMap, "Image_Type_Text", niftiInfo, "ImageTypeText");
		} else {
			imageType = getStringValue(siemensValuesMap, "Image_Type", niftiInfo, "");
		}
		scan.setParameters_imagetype(imageType);

		// Parsing TR/TE/TI values.
		MapFunction<Double, Double> convertNiftiTimingToMilliseconds = (Double v) -> { return (v != null) ? v*1000 : null; };
		Double tr = parseDoubleFromDicomData(
			siemensValuesMap,
			"RepetitionTime",
			niftiInfo,
			"RepetitionTime",
			convertNiftiTimingToMilliseconds);
		scan.setParameters_tr(tr);

		Double te = parseDoubleFromDicomData(
			siemensValuesMap,
			"EchoTime",
			niftiInfo,
			"EchoTime",
			convertNiftiTimingToMilliseconds);
		scan.setParameters_te(te);

		Double ti = parseDoubleFromDicomData(
			siemensValuesMap,
			"InversionTime",
			niftiInfo,
			"InversionTime",
			convertNiftiTimingToMilliseconds);
		scan.setParameters_ti(ti);
	}

	/**
	 * Sets the values on the given scan that are non-specific to
	 * XNAT's scan schema. These values will be applied as `addParam`
	 * tags in the session XML.
	 * @param scan
	 * @param niftiInfo
	 * @param siemensValuesMap
	 */
	private static void updateScanWithAdditionalDicomData(XnatMrscandata scan, List<Map<String, Object>> niftiInfo, Map<String, String> siemensValuesMap) {
		// Adding SoftwareVersion as an `addParam`.
		updateScanWithAddParam(scan, "SoftwareVersion", scan.getScanner_softwareversion());

		// Parsing FmriExternalInfo into fMRI_Version subtypes.
		String FmriExternalInfo = getStringValue(siemensValuesMap, "FmriExternalInfo", niftiInfo, "");
		if (FmriExternalInfo != null && FmriExternalInfo.contains("||")) {
			// Collecting values for:
			// - fMRI_Version_Sequence
			// - fMRI_Version_ICE_Win32
			// - fMRI_Version_ICE_Linux
			String[] fMRIParts = FmriExternalInfo.split("\\|\\|");
			// Filter out the first element which should be filenameUUID.
			if (fMRIParts.length > 0) {
				fMRIParts = Arrays.copyOfRange(fMRIParts, 1, fMRIParts.length);
			}
			// Initialize a list of relations to the above
			// version values we are expecting.
			String[] fMRIVersionNames = new String[]{
					"fMRI_Version_Sequence",
					"fMRI_Version_ICE_Win32",
					"fMRI_Version_ICE_Linux"
			};
			// Loop through declared values and try to set them as
			// addParam fields.
			for (int i = 0; i < fMRIVersionNames.length; i++) {
				String name  = fMRIVersionNames[i]; // Offset by -1 to align with `fMRIParts`.
				String value = fMRIParts[i];
				// Do cleanup of value from parts to remove "<fieldname>: " prefix.
				value = value.replace(String.format("%s: ", name.replace("fMRI_Version_", "")), "");
				updateScanWithAddParam(scan, name, value);
			}
		}

		// Parsing Siemens Coil String.
		String coilString;
		if (scannerTypeIs(scan, "XA")) {
			coilString = getStringValue(siemensValuesMap, "Siemens_Coil_String_XA", niftiInfo, "");
		} else {
			coilString = getStringValue(siemensValuesMap, "Siemens Coil String", niftiInfo, "");
		}
		if (coilString != null && !coilString.isEmpty()) {
			updateScanWithAddParam(scan, "Siemens Coil String", coilString);
		}

		// Parsing BandwidthPerPixelPhaseEncode.
		String bandwidthPerPixelPhaseEncode = getStringValue(
				siemensValuesMap,
				"Siemens Bandwidth per pixel phase encode",
				niftiInfo,
				"BandwidthPerPixelPhaseEncode");
		if (bandwidthPerPixelPhaseEncode != null && !bandwidthPerPixelPhaseEncode.isEmpty()) {
			updateScanWithAddParam(scan, "Siemens Bandwidth per pixel phase encode", bandwidthPerPixelPhaseEncode);
		}

		// Apply Table Position
		String tablePosition;
		if (scannerTypeIs(scan, "XA")) {
			tablePosition = getStringValue(siemensValuesMap, "Siemens_Table_Position_XA", niftiInfo, "");
		} else {
			tablePosition = getStringValue(siemensValuesMap, "Siemens Table Position", niftiInfo, "");
		}
		if (tablePosition != null && !tablePosition.isEmpty()) {
			updateScanWithAddParam(scan, "Siemens Table Position", tablePosition);
		}

		// Dump remaining field data from DICOM map into `addParam`
		// fields.
		final Iterator<Entry<String, String>> i  = siemensValuesMap.entrySet().iterator();
		while (i.hasNext()) {
			final Entry<String, String> e = i.next();
			final String key= e.getKey();
			String value=e.getValue();

			// If the value key represents XA data,
			// but the scanner type is not XA, skip.
			if (key.contains("XA") && !scannerTypeIs(scan, "XA")) {
				i.remove();
				continue;
			}

			// log.debug("key:  " + key + " - 1");
			try {
				String fieldName;
				fieldName = !(key.startsWith("fMRI_Version")) ? key.replaceAll("_", " ") : key;

				if (key.contains("XA")) {
					fieldName = fieldName.replaceAll("[_ ]?XA[0-9]+", "");
					value = value.replaceAll("\"", "");
				}
				updateScanWithAddParam(scan, fieldName, value);
   	            //log.debug("key:  " + key + " - 2");
    	   } catch (Exception ex) {
    	        log.warn("Exception thrown pulling keyField: " + key);
    	   }
    	   i.remove();
		}

		// Set orientation value.  Only set orientation addparam if we don't already have one and string length is > 5.
		final String orientationValue = getStringValue(siemensValuesMap,"",niftiInfo,"ImageOrientationText");
		if (orientationValue != null && orientationValue.length()<=5) {
			scan.setParameters_orientation(orientationValue);
		} else if (orientationValue != null && orientationValue.length()>5) {
				scan.setParameters_orientation(orientationValue.substring(0,5).replaceFirst("[^A-Za-z].*$",""));
				if (!hasOrientationTextField(scan)) {
					updateScanWithAddParam(scan, "Orientation Text", orientationValue);
				}
		}
	}

	private static void updateScanWithAddParam(XnatMrscandata scan, String name, String value) {
		XnatAddfield newField = new XnatAddfield();
		newField.setName(name);
		newField.setAddfield(value);
		try {
			scan.addParameters_addparam(newField);
		} catch (Exception exc) {
			log.warn(String.format("Exception attempting to set %s: %s", name));
			log.error(String.format("Culprit exception:\n%s", exc));
		}
	}

	/**
	 * Attempt to save the data currently set on the `scan`.
	 * @param scan
	 * @param user
	 */
	private static void saveScanWithDicomData(XnatMrscandata scan, UserI user) {
		final PersistentWorkflowI wrk;
		EventMetaI ci = null;
		try {
			wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, scan.getItem(), 
						EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EventUtils.MODIFY_VIA_WEB_SERVICE));
			ci = wrk.buildEvent();
		} catch (JustificationAbsent | ActionNameAbsent | IDAbsent e) {
			log.error("ERROR:  Error creating workflow", e);
		}
		try {
			//log.debug("Scan to save (scan=" + scan.getItem().toXML_String() + ")");
			SaveItemHelper.authorizedSave(scan.getItem(),user,false,true,ci);
			//log.debug("Scan saved (scan=" + scan.getId() + ")");
			//log.debug(scan.getItem().toXML_String());
		} catch (Exception e) {
			log.error("ERROR:  Error saving scan (scan=" + scan.getId() + ")", e);
			log.error(scan.getItem().toXML_String());
		}
	}

	private static boolean scannerTypeIs(XnatMrscandata scan, String type) {
		return scan.getScanner_softwareversion().contains(type);
	}

	private static boolean hasOrientationTextField(XnatMrscandata scan) {
		for (final XnatAddfieldI addParam : scan.getParameters_addparam()) {
			if (addParam.getName().contains("Orientation Text")) {
				return true;
			}
		}
		return false;
	}

	private static String safeString(Object obj) {
		return (obj != null) ? obj.toString() : "";
	}

	private static Double safeDouble(String ins) {
		try {
			return (ins != null) ? Double.valueOf(ins) : null;
		} catch (NumberFormatException e) {
			log.warn("WARNING:  Encountered invalid Double" + ins);
			return null;
		}
	}

	private static Integer safeInteger(String ins) {
		try {
			return (ins != null) ? Integer.valueOf(ins) : null;
		} catch (NumberFormatException e) {
			log.warn("WARNING:  Encountered invalid Integer" + ins);
			return null;
		}
	}

	/**
	 * Representative of some function or method
	 * that performs an operation on the passed
	 * value and then returns that value.
	 */
	interface MapFunction<VT, RT> {
		RT doMap(VT value);
	}
}
