package ccf.nrg.siemens.dicomheaderextractor.client;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.nrg.ccf.common.utilities.components.ScriptResult;
import org.nrg.ccf.common.utilities.utils.ScriptExecUtils;

import ccf.nrg.siemens.dicomheaderextractor.exception.DicomHeaderExtractionException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import lombok.extern.slf4j.Slf4j;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
//import org.springframework.stereotype.Component;
import com.google.gson.reflect.TypeToken;

//@Component
@Slf4j
public class Dcm2niixBidsClient {
	
	private final SimpleDateFormat _formatter = new SimpleDateFormat("yyyyMMddHHmm");
	private final String TEMPDIR_PREFIX = "dcm2niix-bids-";
	// TODO:  We should probably make this configurable
	public final String DCM2NIIX_COMMAND = 
			"/ceph/intradb/singularity/container/run_singularity_chpc3.sh /opt/app/intradb/dcm2niix/2023.04.11/dcm2niix";
	public final String DCM2NIIX_OPTIONS = "-v n -ba y -b o";
	final Gson _gson = new Gson();
	
	public List<Map<String,Object>> getDcm2niixBidsInfo(File dicomDir) throws DicomHeaderExtractionException {
		
		final List<Map<String,Object>> bidsInfoList = new ArrayList<>();
		final File tempDir = getTempDir();
		if (!(dicomDir.exists() && dicomDir.isDirectory() && tempDir.exists() && tempDir.isDirectory())) {
			return bidsInfoList;
		}
		final String scriptCommand = new StringBuffer(DCM2NIIX_COMMAND).append(" ").append(DCM2NIIX_OPTIONS) 
				.append(" -o ").append(tempDir.getAbsolutePath()).append(" ")
				.append(dicomDir.getAbsolutePath()).toString(); 
		final ScriptResult result = ScriptExecUtils.execRuntimeCommand(scriptCommand);
		if (!result.isSuccess()) {
			throw new DicomHeaderExtractionException("DCM2NIIX script execution failure " + result.toString());
		}
		File[] tempFileArray = tempDir.listFiles();
		Arrays.sort(tempFileArray, (File f1, File f2) -> f1.getName().compareToIgnoreCase(f2.getName()));
		for (final File bidsJson : tempFileArray) {
			if (bidsJson.exists()) {
				try {
					final Map<String,Object> jsonMap  = _gson.fromJson(
							FileUtils.readFileToString(bidsJson,Charset.defaultCharset()) ,
							new TypeToken<Map<String,Object>>(){}.getType());
					if (jsonMap != null) {
						bidsInfoList.add(jsonMap);
						//log.debug(jsonMap.toString());
					}
				} catch (JsonSyntaxException | IOException e) {
					throw new DicomHeaderExtractionException("DCM2NIIX json conversion failure " + result.toString());
				}
			}
			
		}
		if (tempDir.exists() && tempDir.isDirectory() && tempDir.getName().startsWith(TEMPDIR_PREFIX)) {
			try {
				FileUtils.deleteDirectory(tempDir);
			} catch (IOException e) {
				// Do nothing
			}
		}
		return bidsInfoList;

	}

	private File getTempDir() throws DicomHeaderExtractionException {
		File tempDir;
		try {
			final String dt = _formatter.format(new Date());
			final String prefix = TEMPDIR_PREFIX + dt + "-";
			tempDir = Files.createTempDirectory(prefix).toFile();
			return tempDir;
		} catch (IOException e) {
			log.error("Exception thrown creating temp directory: ",e);
			throw new DicomHeaderExtractionException(e);
		}
		
	}
	

}
