package ccf.nrg.siemens.dicomheaderextractor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.dcm4che2.io.DicomCodingException;
import org.nrg.framework.annotations.XnatPlugin;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xft.XFTTable;
//import org.nrg.xnat.helpers.dicom.DicomHeaderDump;
import ccf.nrg.siemens.dicomheaderextractor.xnathelpers.DicomHeaderDump_XA30;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import au.com.bytecode.opencsv.CSVReader;
import ccf.nrg.siemens.dicomheaderextractor.util.DicomConstants;
import ccf.nrg.siemens.dicomheaderextractor.util.DicomHeaderUtility;
import ccf.nrg.siemens.dicomheaderextractor.util.ImageComments;

/**
 * The Class SiemensDicomHeaderExtractor.
 * @author Atul
 */
@XnatPlugin(value = "dicomHeaderExtractionPlugin",
name = "Dicom HeaderExtraction Plugin",
log4jPropertiesFile="/dicomExtractionLog4j.properties")
public class SiemensDicomHeaderExtractor {

	/** The logger. */
	public static Logger logger =  LoggerFactory.getLogger(SiemensDicomHeaderExtractor.class);

	/** The siemens key map. */
	static Map<String, String> siemensKeyMap = new LinkedHashMap<String, String>();
	
	/** The use acquisition number. */
	Boolean useAcquisitionNumber = true;

	static {
		CSVReader reader = null;
		try {
			reader = new CSVReader(new InputStreamReader(
					SiemensDicomHeaderExtractor.class.getResourceAsStream("/SiemensHeaders.csv")));
			String[] nextLine;
			while ((nextLine = reader.readNext()) != null) {
				siemensKeyMap.put(nextLine[0], nextLine[1]);
			}
		} catch (IOException e) {
			logger.error("Unable to load Siemens Headers from SiemensHeaders.csv" + e.getMessage());
		} finally {
			if (reader != null)
				try {
					reader.close();
				} catch (IOException e) {
					logger.error("Unable to load SiemensHeaders.csv file");
					logger.error(e.getMessage());
					e.printStackTrace();
				}
		}
	}

	/**
	 * Gets the siemens dicom header info map.
	 *
	 * @param dcmDirPath
	 *            the dcm dir path
	 * @return the siemens dicom header info map
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 * @throws ParseException 
	 * @throws Exception 
	 */
	public Map<String, String> getSiemensDicomHeaderInfoMap(String dcmDirPath) throws Exception {
		logger.debug("Extracting Dicom headers for the files present under "+dcmDirPath);
		Map<String, String> siemensValuesMap = new LinkedHashMap<String, String>();
		try {
			// As per Mike Harms comments, modified plugin to extract information from the dicom file where Image/Instance Number=1.
			String earliestAcqTimeDicomFilePath = null;
			
			// Get all the files with .dcm extension
			File[] filesWithDCMExtension = new File(dcmDirPath).listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.endsWith(".dcm");
				}
			});
			// Get all the files without any extension. Special use-case for UCLA files.
			File[] filesWithoutExtension = new File(dcmDirPath).listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return !name.endsWith(".dcm") && !name.endsWith(".xml");
				}
			});
			Map<Integer,ImageComments> imageComments=new TreeMap<Integer,ImageComments>();

			logger.debug("Extracting information from all dicom files");
			// Process All dicom files for specific attributes
			if (filesWithDCMExtension != null && filesWithDCMExtension.length >0)
				earliestAcqTimeDicomFilePath=getAttributesFromAllFiles(filesWithDCMExtension, siemensValuesMap,imageComments);

			if (filesWithoutExtension != null && filesWithoutExtension.length >0)
			{
				if(earliestAcqTimeDicomFilePath==null)
					earliestAcqTimeDicomFilePath=getAttributesFromAllFiles(filesWithoutExtension, siemensValuesMap,imageComments);
				else
					getAttributesFromAllFiles(filesWithoutExtension, siemensValuesMap,imageComments);
			}

			if (filesWithDCMExtension != null && filesWithDCMExtension.length > 0)
				processFile(earliestAcqTimeDicomFilePath!=null?earliestAcqTimeDicomFilePath:filesWithDCMExtension[0].getPath(), siemensValuesMap);
			if (filesWithoutExtension != null && filesWithoutExtension.length > 0)
				processFile(earliestAcqTimeDicomFilePath!=null?earliestAcqTimeDicomFilePath:filesWithoutExtension[0].getPath(), siemensValuesMap);

			setImageCommentsToResult(siemensValuesMap, imageComments);
			//printDicomHeaders(siemensValuesMap);
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			logger.error(ExceptionUtils.getStackTrace(e));
			throw e;
		} catch (IOException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			logger.error(ExceptionUtils.getStackTrace(e));
			throw e;
		}
		catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			logger.error(ExceptionUtils.getStackTrace(e));
			throw e;
		}
		logger.debug("Returning HeaderInfo Map");
		return siemensValuesMap;
	}

	
	
	/**
	 * Gets the attributes from all files.
	 *
	 * @param files the files
	 * @param siemensValuesMap the siemens values map
	 * @param imageComments the image comments
	 * @return the attributes from all files
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParseException the parse exception
	 */
	private String getAttributesFromAllFiles(File[] files, Map<String, String> siemensValuesMap,Map<Integer,ImageComments> imageComments)
			throws FileNotFoundException, IOException, ParseException {
		Map<Integer, Integer> acquisitionVsInstanceNumberMap = new HashMap<Integer, Integer>();
		String earliestAcqTimeDicomFilePath=null;

		for (int i = 0; i < files.length; i++) {
			Map<String, String> headerValuesMap = getAllHeadersMap(files[i].getPath(), Boolean.FALSE);
			if(headerValuesMap!=null)
			{
				getEarliestAcqTimeValue(siemensValuesMap, headerValuesMap);
				getDiffusionBValue(siemensValuesMap, headerValuesMap, "VE");
				getDiffusionBValue(siemensValuesMap, headerValuesMap, "XA");
				getImageCommentsValue(imageComments, headerValuesMap, acquisitionVsInstanceNumberMap);
				if(earliestAcqTimeDicomFilePath==null)
					try {
						earliestAcqTimeDicomFilePath=Integer.valueOf(headerValuesMap.get(DicomConstants.INSTANCE_NUMBER_TAG))==1?files[i].getPath():null;
					} catch (NumberFormatException e) {
						// Do nothing, just continue
					}
			}
		}
		return earliestAcqTimeDicomFilePath;
	}


	/**
	 * Sets the image comments to result.
	 *
	 * @param siemensValuesMap the siemens values map
	 * @param imageCommentsMap the image comments map
	 */
	private void setImageCommentsToResult(Map<String, String> siemensValuesMap, Map<Integer,ImageComments> imageCommentsMap) {
		Boolean uniqueComments=true;
		if(!imageCommentsMap.isEmpty())
		{
			Set<String> commentsValueSet= new HashSet<String>();
			for (Iterator<ImageComments> iterator = imageCommentsMap.values().iterator(); iterator.hasNext();) {
				ImageComments imageComments =  iterator.next();
				if(!commentsValueSet.isEmpty() &&!commentsValueSet.contains(imageComments.getValue()))
				{
					uniqueComments=false;
					break;
				}
				else
				{
					commentsValueSet.add(imageComments.getValue());
				}
			}
			if(uniqueComments)
			{
				siemensValuesMap.put(DicomConstants.IMAGE_COMMENTS_KEY, ((ImageComments)imageCommentsMap.get(imageCommentsMap.keySet().iterator().next())).getValue());
			}
			else 
			{
				siemensValuesMap.put(DicomConstants.IMAGE_COMMENTS_KEY, "<b>View/Download XML to view image comments.</b>");
				Iterator<Integer> iter= imageCommentsMap.keySet().iterator();
				while(iter.hasNext())
				{
					Integer index=iter.next();
					siemensValuesMap.put("Image Comments[" + index + "]", ((ImageComments)imageCommentsMap.get(index)).getValue());
				}
			}
		}
	}

	/**
	 * Gets the earliest acq time value.
	 *
	 * @param siemensValuesMap the siemens values map
	 * @param headerValuesMap the header values map
	 * @return the earliest acq time value
	 * @throws ParseException the parse exception
	 */
	private void getEarliestAcqTimeValue(Map<String, String> siemensValuesMap, Map<String, String> headerValuesMap) throws ParseException {
		String acqTimeFromDicom=headerValuesMap.get(DicomConstants.ACQUISITION_TIME_TAG);
		String acqTimeFromExistingData=siemensValuesMap.get(DicomConstants.ACQUISITION_TIME_KEY);
		SimpleDateFormat format= new SimpleDateFormat("HH:mm:ss");
		Date dateFromDicom=null;
		Date dateFromHeaderMap=null;
		if(acqTimeFromDicom!=null && !DicomConstants.SPACE_STRING.equals(acqTimeFromDicom))
			dateFromDicom=format.parse(acqTimeFromDicom);
		if(acqTimeFromExistingData!=null && !DicomConstants.SPACE_STRING.equals(acqTimeFromExistingData))
			dateFromHeaderMap=format.parse(acqTimeFromExistingData);
		
		if (dateFromHeaderMap ==null && dateFromDicom != null) {
			siemensValuesMap.put(DicomConstants.ACQUISITION_TIME_KEY,acqTimeFromDicom);
		} 
		else if (dateFromDicom != null
				&& dateFromHeaderMap != null
				&& dateFromDicom.before(dateFromHeaderMap)) {
			siemensValuesMap.put(DicomConstants.ACQUISITION_TIME_KEY,acqTimeFromDicom);
		}
	}

	/**
	 * Gets the diffusion B value and sets it to the siemensValuesMap.
	 *
	 * @param siemensValuesMap the siemens values map
	 * @param headerValuesMap the header values map
     */
	private void getDiffusionBValue(Map<String, String> siemensValuesMap, Map<String, String> headerValuesMap, String scanType) {
		if (shouldOverwriteSiemensBValue(siemensValuesMap, headerValuesMap, scanType)) {
			String key = getDiffusionBKeyByType(scanType);
			// "UNKNOWN" should not be set as a
			// key in the `siemensValuesMap`.
			if (key.equals(DicomConstants.UNKNOWN))
				return;
			siemensValuesMap.put(key, getDiffusionBValueByTag(headerValuesMap, scanType));
		}
	}

	/**
	 * Determines the order of whether the Diffusion B value should be
	 * overwritten from the `headersValuesMap` or the `siemensValuesMap`.
	 * @param siemensValuesMap
	 * @param headerValuesMap
	 */
	private boolean shouldOverwriteSiemensBValue(Map<String, String> siemensValuesMap, Map<String, String> headerValuesMap, String scanType) {
		if (!diffusionBValueByTagExists(headerValuesMap, scanType))
			return false;

		Double fromHeaders = Double.valueOf(getDiffusionBValueByTag(headerValuesMap, scanType));
		// The value we are looking for is not yet set on
		// the Siemens map, but we need to ensure the value
		// is valid.
		if (!diffusionBValueByKeyExists(siemensValuesMap, scanType) && !fromHeaders.equals(0.0))
			return true;
		// At this point we know a value exists
		// in the Headers map. We need to apply
		// this value to the Siemens mapping.
		else if (!diffusionBValueByKeyExists(siemensValuesMap, scanType))
			return true;
		Double fromSiemens = Double.valueOf(getDiffusionBValueByKey(siemensValuesMap, scanType));
		// If the value from Siemens Mapping is
		// less than the value from the Headers,
		// and the headers value is not 0.0, then
		// we set the value.
		if (fromSiemens < fromHeaders && !fromHeaders.equals(0.0))
			return true;

		return false;
	}

	/**
	 * Determines if the Diffusion B value is
	 * available in the given mapping as the
	 * DICOM declared key.
	 * @param map
	 * @return
	 */
	private boolean diffusionBValueByKeyExists(Map<String, String> map, String scanType) {
		return !valueIsEmptyOrUnknown(getDiffusionBValueByKey(map, scanType));
	}

	/**
	 * Determines if the Diffusion B value is
	 * available in the given mapping as the
	 * DICOM declared tag.
	 * @param map
	 * @return
	 */
	private boolean diffusionBValueByTagExists(Map<String, String> map, String scanType) {
		return !valueIsEmptyOrUnknown(getDiffusionBValueByTag(map, scanType));
	}

	/**
	 * Retrieves the Diffusion B value from the
	 * given mapping using the `DiffusionBValue`
	 *  key. Potentially returns `"UNKNOWN"` to
	 * avoid issues with `NullPointerException`s.
	 * @return
	 */
	private String getDiffusionBValueByKey(Map<String, String> map, String scanType) {
		String retrieved = map.get(getDiffusionBKeyByType(scanType));
		return (retrieved != null) ? retrieved : DicomConstants.UNKNOWN;
	}

	/**
	 * Retrieves the Diffusion B value from the
	 * given mapping using the `DiffusionBValue`
	 * tag. Potentially returns `"UNKNOWN"` to
	 * avoid issues with `NullPointerException`s.
	 */
	private String getDiffusionBValueByTag(Map<String, String> map, String scanType) {
		String retrieved = map.get(getDiffusionBTagByType(scanType));
		return (retrieved != null) ? retrieved : DicomConstants.UNKNOWN;
	}

	/**
	 * Retrieve the tag which should be available
	 * in some DICOM header mapping based on the
	 * scan type. If the scan type is not
	 * supported, or is not recognized, return
	 * "UNKNOWN".
	 * @param scanType
	 */
	private String getDiffusionBKeyByType(String scanType) {
		switch (scanType) {
		case "XA":
			return DicomConstants.DIFFUSION_B_KEY_XA;
		case "VE":
			return DicomConstants.DIFFUSION_B_KEY_VE;
		default:
			return DicomConstants.UNKNOWN;
		}
	}

	/**
	 * Retrieve the key which should be available
	 * in some DICOM header mapping based on the
	 * scan type. If the scan type is not
	 * supported, or is not recognized, return
	 * "UNKNOWN".
	 * @param scanType
	 */
	private String getDiffusionBTagByType(String scanType) {
		switch (scanType) {
		case "XA":
			return DicomConstants.DIFFUSION_B_TAG_XA;
		case "VE":
			return DicomConstants.DIFFUSION_B_TAG_VE;
		default:
			return DicomConstants.UNKNOWN;
		}
	}

	/**
	 * Gets the image comments value.
	 *
	 * @param imageComments the image comments
	 * @param headerValuesMap the header values map
	 * @param acquisitionVsInstanceNumberMap the acquisition vs instance number map
	 * @return the image comments value
	 */
	private void getImageCommentsValue(Map<Integer,ImageComments> imageComments, Map<String, String> headerValuesMap,
			Map<Integer, Integer> acquisitionVsInstanceNumberMap) {
		if (headerValuesMap.get(DicomConstants.IMAGE_COMMENTS_TAG) != null) {
			//		&& imageCommentsMap.get(headerValuesMap.get(DicomConstants.IMAGE_COMMENTS_TAG)) == null) {
				/**
				 * Use Acquisition number for index if it is unique for every dicom file.
				 * Otherwise, use Instance number.
				 */
				if (useAcquisitionNumber) {
					Integer acqNumKey;
					if (headerValuesMap.get(DicomConstants.ACQUISITION_NUMBER_TAG) != null) {
						acqNumKey = Integer.valueOf(headerValuesMap.get(DicomConstants.ACQUISITION_NUMBER_TAG));
					} else {
						acqNumKey = null;
					}
					if (acqNumKey != null && acquisitionVsInstanceNumberMap.containsKey(acqNumKey)) {
						useAcquisitionNumber = false;
						imageComments=replaceAcqNumWithInstanceNum(imageComments, acquisitionVsInstanceNumberMap);
					} else if (acqNumKey != null) {
						acquisitionVsInstanceNumberMap.put(
								Integer.valueOf(headerValuesMap.get(DicomConstants.ACQUISITION_NUMBER_TAG)),
								Integer.valueOf(headerValuesMap.get(DicomConstants.INSTANCE_NUMBER_TAG)));
					} else {
						useAcquisitionNumber = false;
					}
				}
				if (useAcquisitionNumber)
					imageComments.put(Integer.valueOf(headerValuesMap.get(DicomConstants.ACQUISITION_NUMBER_TAG)),new ImageComments(Integer.valueOf(headerValuesMap.get(DicomConstants.ACQUISITION_NUMBER_TAG)),escapeImageComments(headerValuesMap.get(DicomConstants.IMAGE_COMMENTS_TAG))));
				else
					imageComments.put(Integer.valueOf(headerValuesMap.get(DicomConstants.INSTANCE_NUMBER_TAG)),new ImageComments(Integer.valueOf(headerValuesMap.get(DicomConstants.INSTANCE_NUMBER_TAG)),escapeImageComments(headerValuesMap.get(DicomConstants.IMAGE_COMMENTS_TAG))));
		}
		
	}
	
	/**
	 * Escape image comments.
	 *
	 * @param imageComments the image comments
	 * @return the string
	 */
	private String escapeImageComments(String imageComments)
	{
		return imageComments.replaceAll("", "?");
	}
	
	/**
	 * Replace acq num with instance num.
	 *
	 * @param imageCommentsMap the image comments map
	 * @param acquisitionVsInstanceNumberMap the acquisition vs instance number map
	 * @return the map
	 */
	private Map<Integer,ImageComments> replaceAcqNumWithInstanceNum(Map<Integer,ImageComments> imageCommentsMap,
			Map<Integer, Integer> acquisitionVsInstanceNumberMap) {
		Map<Integer,ImageComments>  tempMap=new HashMap<Integer,ImageComments> (imageCommentsMap.size());

		for (Iterator<Integer> iterator = acquisitionVsInstanceNumberMap.keySet().iterator(); iterator.hasNext();) {
			Integer acqNumber = iterator.next();
			tempMap.put(acquisitionVsInstanceNumberMap.get(acqNumber).intValue(),
					new ImageComments(acquisitionVsInstanceNumberMap.get(acqNumber).intValue(), 
							((ImageComments)imageCommentsMap.get(acqNumber)).getValue()));
		}
		return tempMap;
	}

	/**
	 * Process file.
	 *
	 * @param filePath
	 *            the file path
	 * @param siemensValuesMap
	 *            the siemens values map
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws ParseException 
	 */
	private void processFile(String filePath, Map<String, String> siemensValuesMap)
			throws FileNotFoundException, IOException, ParseException {
		Map<String, String> headerValuesMap = getAllHeadersMap(filePath, Boolean.TRUE);
		if (headerValuesMap != null)
			getSiemensHeadersMap(headerValuesMap, siemensValuesMap);
	}

	/**
	 * Gets the all headers map.
	 *
	 * @param filePath
	 *            the file path
	 * @return the all headers map
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws ParseException 
	 */
	private Map<String, String> getAllHeadersMap(String filePath, Boolean skipMultiFileAttributes) throws IOException, FileNotFoundException, ParseException {
		Map<String, String> headerValuesMap=null;
		try {
		DicomHeaderDump_XA30 d = new DicomHeaderDump_XA30(filePath);
		XFTTable xft = d.render();
		DicomHeaderUtility util = new DicomHeaderUtility();
		headerValuesMap = util.createHeaderMap(xft,skipMultiFileAttributes);
		}catch(DicomCodingException e){
			logger.error("Not a valid dicom file : "+filePath);
		}
		return headerValuesMap;
	}

	/**
	 * Gets the siemens headers map.
	 *
	 * @param headerValuesMap
	 *            the header values map
	 * @param siemensValuesMap
	 *            the siemens values map
	 * @return the siemens headers map
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void getSiemensHeadersMap(Map<String, String> headerValuesMap, Map<String, String> siemensValuesMap)
			throws IOException {

		for (Iterator<String> iterator = siemensKeyMap.keySet().iterator(); iterator.hasNext();) {
			String key = iterator.next();

			if (headerValuesMap.get(key) != null && !DicomConstants.EMPTY_STRING.equals(headerValuesMap.get(key))) {
				// Fixes a bug where values for DIFFUSION_B were being
				// overwritten by invalid values.
				// e.g. we would expect "3000.0", but the headers map
				// would contain a value of "0.0" and overwrite the
				// valid value with the incorrect one.
				if (DicomConstants.DIFFUSION_B_TAG_XA.equals(key) && !shouldOverwriteSiemensBValue(siemensValuesMap, headerValuesMap, "XA"))
					continue;
				siemensValuesMap.put(
						siemensKeyMap.get(key).replaceAll(DicomConstants.SPACE_STRING, DicomConstants.UNDERSCORE), headerValuesMap.get(key));
			}
		}
	}

	/**
	 * Send notifications.
	 *
	 * @param subject
	 *            the subject
	 * @param message
	 *            the message
	 * @param emailAddress
	 *            the email address
	 */
	public void sendNotifications(String subject, String message, String[] emailAddress) {
		AdminUtils.sendUserHTMLEmail(subject, message, false, emailAddress);
	}
	
	/**
	 * Prints the dicom headers.
	 *
	 * @param valuesMap
	 *            the values map
	 */
	private void printDicomHeaders(Map<String, String> valuesMap) {
		for (Iterator<String> iterator = valuesMap.keySet().iterator(); iterator.hasNext();) {
			String key = iterator.next();
			System.out.println(key + " :: " + valuesMap.get(key));
		}
	}

	/**
	 * The passed string value is either an empty
	 * string or is "UNKNOWN".
	 * @param value
	 * @return
	 */
	private boolean valueIsEmptyOrUnknown(String value) {
		String cleanedValue = value.trim();
		return (DicomConstants.EMPTY_STRING.equals(cleanedValue) || DicomConstants.UNKNOWN.equals(cleanedValue));
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 * @throws Exception
	 *             the exception
	 */
	public static void main(String... args) throws Exception {
		SiemensDicomHeaderExtractor s = new SiemensDicomHeaderExtractor();
		s.getSiemensDicomHeaderInfoMap("C:\\Users\\Atul\\Desktop\\HCD0184743_V1_A\\scans\\17_T1w_4e\\DICOM");
	}
}