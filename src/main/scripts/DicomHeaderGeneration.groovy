import ccf.nrg.siemens.dicomheaderextractor.SiemensDicomHeaderExtractor
import org.nrg.xdat.om.XnatExperimentdata
import org.nrg.xdat.om.XnatResourcecatalog
import org.nrg.xdat.om.XnatImagescandata
import groovy.io.FileType
import java.lang.reflect.Method
import org.nrg.xdat.om.XnatAddfield
import org.nrg.xdat.bean.XnatImagescandataBean
import org.nrg.xft.utils.SaveItemHelper
import org.nrg.xdat.om.XnatMrscandata
import org.nrg.automation.runners.GroovyScriptRunner
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.apache.commons.lang.exception.ExceptionUtils

def _logger = LoggerFactory.getLogger(GroovyScriptRunner.class)
def dicomHeaderGen= new SiemensDicomHeaderExtractor()
def _failure= false
def _exception=""
def basedir

    
def experimentData = XnatExperimentdata.getXnatExperimentdatasById(dataId, user, false)
def scans=experimentData.getSortedScans()
for (scan in scans) 
{
    if(scan instanceof XnatMrscandata)
    {
        try {
            def resources= scan.getFile()
            for(resource in resources)
            {
                def type = resource.getLabel()
                if("DICOM"== type || "secondary"==type)
                {
                	def dicomOrigDir="/data/intradb/archive/"+externalId.toString()+"/arc001/"+experimentData.getLabel()+"/SCANS/"+scan.getId()+"/DICOM_ORIG"
                    def dicomDir="/data/intradb/archive/"+externalId.toString()+"/arc001/"+experimentData.getLabel()+"/SCANS/"+scan.getId()+"/DICOM"
					def secondaryDir="/data/intradb/archive/"+externalId.toString()+"/arc001/"+experimentData.getLabel()+"/SCANS/"+scan.getId()+"/secondary"
                    basedir = new File(dicomOrigDir)	//Use DICOM_ORIG if directory exists
                    if(!basedir.exists())
                    	basedir = new File(dicomDir)	//Use DICOM if directory exists
					if(!basedir.exists())
						basedir = new File(secondaryDir)//Use secondary if directory exists
                    _logger.debug(basedir.getAbsolutePath())
					if(basedir.exists())
					{
						def siemensValuesMap=dicomHeaderGen.getSiemensDicomHeaderInfoMap(dicomDir);
						processData(scan,siemensValuesMap)
					}
					else
					{
						_logger.error("Scan directories does not exists for "+basedir)
					}
                }
            }
        }
        catch (Exception e) {
	        _failure=true
            _logger.error("Dicom Extraction threw exception "+basedir+"\n"+ ExceptionUtils.getFullStackTrace(e))
            _exception= _exception+"<br><b>"+"Dicom Extraction threw exception for "+basedir+"</b><br>"+ ExceptionUtils.getFullStackTrace(e)
        }
    }
}
if(_failure)
{ 
    dicomHeaderGen.sendNotifications("Dicom Extraction failed :: "+experimentData.getLabel(),_exception,("akaushal@wustl.edu").split())
    throw new Exception("Dicom Extraction process failed for "+experimentData.getLabel())
}

def processData(scan,siemensValuesMap)
{
    def _logger = LoggerFactory.getLogger(GroovyScriptRunner.class)
    _logger.error("processData - scan " + scan.getId());
   try { 
        _logger.error("Series_Description - 1");
        if(siemensValuesMap.get("Series_Description")!=null) {
               if  (scan.getSeriesDescription() == null || scan.getSeriesDescription().toString().trim().equals("")) {
                  scan.setSeriesDescription(siemensValuesMap.get("Series_Description"));
               }
            siemensValuesMap.remove("Series_Description")
            _logger.error("Series_Description - 2");
        }
   } catch (Exception e2) {
        _logger.error("Exception thrown pulling Series_Description");
   }
   try { 
        _logger.error("Modality - 1")
        if(siemensValuesMap.get("Modality")!=null) {
            if (scan.getModality() == null || scan.getModality().toString().trim().equals("")) {
               scan.setModality(siemensValuesMap.get("Modality"));
            }
            siemensValuesMap.remove("Modality")
            _logger.error("Modality - 2")
        }
   } catch (Exception e2) {
        _logger.error("Exception thrown pulling Modality");
   }
   try { 
        _logger.error("Acquisition_Time - 1")
        if(siemensValuesMap.get("Acquisition_Time")!=null)
        {
            scan.setStarttime(siemensValuesMap.get("Acquisition_Time"));
            siemensValuesMap.remove("Acquisition_Time")
            _logger.error("Acquisition_Time - 2")
        }
   } catch (Exception e2) {
        _logger.error("Exception thrown pulling Acquisition_Time");
   }
   try { 
        _logger.error("Acquisition_type - 1")
        if(siemensValuesMap.get("Acquisition_type")!=null)
        {
            scan.setParameters_acqtype(siemensValuesMap.get("Acquisition_type"));
            siemensValuesMap.remove("Acquisition_type")
            _logger.error("Acquisition_type - 2")
        }
   } catch (Exception e2) {
        _logger.error("Exception thrown pulling Acquisition_type");
   }
   try { 
        _logger.error("Scan_sequence - 1")
        if(siemensValuesMap.get("Scan_sequence")!=null)
        {
            scan.setParameters_scansequence(siemensValuesMap.get("Scan_sequence"));;
            siemensValuesMap.remove("Scan_sequence")
            _logger.error("Scan_sequence - 2")
        }
   } catch (Exception e2) {
        _logger.error("Exception thrown pulling Scan_sequence");
   }
   try { 
        _logger.error("Sequence_variant - 1")
        if(siemensValuesMap.get("Sequence_variant")!=null)
        {
            scan.setParameters_seqvariant(siemensValuesMap.get("Sequence_variant"));
            siemensValuesMap.remove("Sequence_variant")
            _logger.error("Sequence_variant - 2")
        }
   } catch (Exception e2) {
        _logger.error("Exception thrown pulling Sequence_variant");
   }
   try { 
        _logger.error("Scan_options - 1")
        if(siemensValuesMap.get("Scan_options")!=null)
        {
            scan.setParameters_scanoptions(siemensValuesMap.get("Scan_options"));
            siemensValuesMap.remove("Scan_options")
            _logger.error("Scan_options - 2")
        }
   } catch (Exception e2) {
        _logger.error("Exception thrown pulling Scan_options");
   }
   try { 
        _logger.error("Subject_position - 1")
        if(siemensValuesMap.get("Subject_position")!=null)
        {
            scan.setParameters_subjectposition(siemensValuesMap.get("Subject_position"));
            siemensValuesMap.remove("Subject_position")
            _logger.error("Subject_position - 2")
        }
   } catch (Exception e2) {
        _logger.error("Exception thrown pulling Subject_position");
   }
   try { 
        _logger.error("Pixel_bandwidth - 1")
        if(siemensValuesMap.get("Pixel_bandwidth")!=null)
        {
            scan.setParameters_pixelbandwidth(new Double(siemensValuesMap.get("Pixel_bandwidth")));
            siemensValuesMap.remove("Pixel_bandwidth")
            _logger.error("Pixel_bandwidth - 2")
        }
   } catch (Exception e2) {
        _logger.error("Exception thrown pulling Pixel_bandwidth");
   }
   try { 
        _logger.error("In-plane_phase_encoding_direction - 1")
        if(siemensValuesMap.get("In-plane_phase_encoding_direction")!=null)
        {
            scan.setParameters_inplanephaseencoding_direction(siemensValuesMap.get("In-plane_phase_encoding_direction"));
            siemensValuesMap.remove("In-plane_phase_encoding_direction")
            _logger.error("In-plane_phase_encoding_direction - 2")
        }
   } catch (Exception e2) {
        _logger.error("Exception thrown pulling In-plane_phase_encoding_direction");
   }
   try { 
        _logger.error("In-plane_phase_encoding_rotation - 1")
        if(siemensValuesMap.get("In-plane_phase_encoding_rotation")!=null)
        {
            scan.setParameters_inplanephaseencoding_rotation(siemensValuesMap.get("In-plane_phase_encoding_rotation"));
            siemensValuesMap.remove("In-plane_phase_encoding_rotation")
            _logger.error("In-plane_phase_encoding_rotation - 2")
        }
   } catch (Exception e2) {
        _logger.error("Exception thrown pulling In-plane_phase_encoding_rotation");
   }
   try { 
        _logger.error("Readout_sample_spacint - 1")
        if(siemensValuesMap.get("Readout_sample_spacing")!=null)
        {
            scan.setParameters_readoutsamplespacing(new Double(siemensValuesMap.get("Readout_sample_spacing")).toString());
            siemensValuesMap.remove("Readout_sample_spacing")
            _logger.error("Readout_sample_spacint - 2")
        }
   } catch (Exception e2) {
        _logger.error("Exception thrown pulling Readout_sample_spacing");
   }
   try { 
        _logger.error("Echo_Spacing_(sec) - 1")
        if(siemensValuesMap.get("Echo_Spacing_(sec)")!=null)
        {
            scan.setParameters_echospacing(new Double(siemensValuesMap.get("Echo_Spacing_(sec)")));
            siemensValuesMap.remove("Echo_Spacing_(sec)")
            _logger.error("Echo_Spacing_(sec) - 2")
        }
   } catch (Exception e2) {
        _logger.error("Exception thrown pulling Echo_Spacing_(sec)");
   }
   try { 
        _logger.error("PhaseEncoding_direction_positive - 1")
        if(siemensValuesMap.get("PhaseEncoding_direction_positive")!=null)
        {
            scan.setParameters_inplanephaseencoding_directionpositive(siemensValuesMap.get("PhaseEncoding_direction_positive"));
            siemensValuesMap.remove("PhaseEncoding_direction_positive")
            _logger.error("PhaseEncoding_direction_positive - 2")
        }
   } catch (Exception e2) {
        _logger.error("Exception thrown pulling PhaseEncoding_direction_positive");
   }
   try { 
        _logger.error("adFlipAngleDegree[1] - 1")
        if(siemensValuesMap.get("adFlipAngleDegree[1]")!=null)
        {
            scan.setParameters_diffusion_refocusflipangle(siemensValuesMap.get("adFlipAngleDegree[1]"));
            siemensValuesMap.remove("adFlipAngleDegree[1]")
            _logger.error("adFlipAngleDegree[1] - 2")
        }
   } catch (Exception e2) {
        _logger.error("Exception thrown pulling adFlipAngleDegree[1]");
   }
   try { 
        _logger.error("Software_Version(s) - 1")
        if(siemensValuesMap.get("Software_Version(s)")!=null)
        {
            scan.setScanner_softwareversion(siemensValuesMap.get("Software_Version(s)"));
            siemensValuesMap.remove("Software_Version(s)")
            _logger.error("Software_Version(s) - 2")
        }
   } catch (Exception e2) {
        _logger.error("Exception thrown pulling Software_Version(s)");
   }
   try { 
        _logger.error("coil - 1")
        if(siemensValuesMap.get("coil")!=null)
        {
            scan.setCoil(siemensValuesMap.get("coil").replaceAll('\"',''));
            siemensValuesMap.remove("coil")
            _logger.error("coil - 2")
        }
   } catch (Exception e2) {
        _logger.error("Exception thrown pulling coil");
   }
   try { 
        _logger.error("UUID - 1")
        if(siemensValuesMap.get("UUID")!=null)
        {
            scan.setFilenameuuid(siemensValuesMap.get("UUID"));
            siemensValuesMap.remove("UUID")
            _logger.error("UUID - 2")
        }
   } catch (Exception e2) {
        _logger.error("Exception thrown pulling UUID");
   }
   try { 
        _logger.error("Diffusion_maximum_b - 1")
        if(siemensValuesMap.get("Diffusion_maximum_b")!=null)
        {
            scan.setParameters_diffusion_bmax(siemensValuesMap.get("Diffusion_maximum_b"));
            siemensValuesMap.remove("Diffusion_maximum_b")
            _logger.error("Diffusion_maximum_b - 2")
        }
   } catch (Exception e2) {
        _logger.error("Exception thrown pulling Diffusion_maximum_b");
   }
   try { 
        _logger.error("Siemens_sSliceArray.... - 1")
        if(siemensValuesMap.get("Siemens_sSliceArray.asSlice[0].dInPlaneRot")!=null)
        {
            scan.setParameters_inplanephaseencoding_rotation(siemensValuesMap.get("Siemens_sSliceArray.asSlice[0].dInPlaneRot"));
            _logger.error("ASliceArray.... - 2")
        }
   } catch (Exception e2) {
        _logger.error("Exception thrown pulling Siemens_sSliceArray.asSlice[0].dInPlaneRot");
   }
   try { 
        _logger.error("Repetition_Time - 1")
        if(siemensValuesMap.get("Repetition_Time")!=null)
        {
            if(scan.getParameters_tr()==null)
                scan.setParameters_tr(new Double(siemensValuesMap.get("Repetition_Time")));
            else
                siemensValuesMap.remove("Repetition_Time")
            _logger.error("Repetition_Time - 2")
        }
   } catch (Exception e2) {
        _logger.error("Exception thrown pulling Repetition_Time");
   }
   try { 
        _logger.error("Inversion_Time - 1")
        if(siemensValuesMap.get("Inversion_Time")!=null)
        {
            if(scan.getParameters_ti()==null)
                scan.setParameters_ti(new Double(siemensValuesMap.get("Inversion_Time")));
            else
                siemensValuesMap.remove("Inversion_Time")
            _logger.error("Inversion_Time - 2")
        }
   } catch (Exception e2) {
        _logger.error("Exception thrown pulling Inversion_Time");
   }
        for(e in siemensValuesMap)
        {
            def key="${e.key}"
            def value="${e.value}"
            _logger.error("key:  " + key + " - 1")
   try { 
            XnatAddfield newField = new XnatAddfield();
            if(!key.startsWith("fMRI_Version"))
                newField.setName(key.replaceAll("_"," "));
            else
                newField.setName(key);
            newField.setAddfield(value);
            scan.addParameters_addparam(newField);
            _logger.error("key:  " + key + " - 2")
   } catch (Exception ex) {
        _logger.error("Exception thrown pulling keyField: " + key);
   }
            siemensValuesMap.remove("${e.key}")
        }
	_logger.error("Saving scan item! (scan=" + scan.getId() + ")");
         SaveItemHelper.authorizedSave(scan.getItem(),user,false,false,null)
}
