# DICOM Header Extraction #
An XNAT plugin for moving DICOM values into scan XML

## Development ##
The following diagram explains the code flow of how this plugin works internally.
This is to provide clarity on how, as it stands, what happens in the background at
runtime on a targeted session for this process.
![Architecture](assets/DicomHeaderExtraction_arch.png)

### IntraDB dependencies ###
In order to develop this project a number of dependencies developed by the IntraDB
are required. At this time, please contact us to retrieve those assets. You will
be able to then copy these files into your `.m2` directory, usually located in your
**home** directory `e.g. /home/your_username/.m2`.

### Gradle ###
This project is developed using `Gradle` and is required to use a version no later
than `Gradle` version 6. Optionally, the gradle wrapper scripts found at the root of
this project already point to version 6 and should limit the need for further
installation.

### Building this Project ###
Once the above requirements are satisfied, you should be able to run the following:
```bash
$ gradle jar
```

For iterative development, it is recommended to chain the `clean` subcommand as well
to remove **dirty** artifacts from the previous build.
```bash
$ gradle clean jar
```
